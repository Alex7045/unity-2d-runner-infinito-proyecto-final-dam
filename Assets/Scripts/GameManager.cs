using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static int coinScore = 0;
    public static int totalScore = 0;
    public static int enemyScore = 0;

    public static bool isDead = false;
    public static bool isStarting = true;
}
