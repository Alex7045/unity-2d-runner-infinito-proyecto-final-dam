using System.Collections.Generic;
using UnityEngine;

public class Pooler : MonoBehaviour
{
    public GameObject platform;
    public int platformsToPooled;
    List<GameObject> platforms = new List<GameObject>();

    // Start is called before the first frame update
    void Start()
    {
        platforms.ForEach(delegate (GameObject i)
        {
            GameObject plat = (GameObject)Instantiate(platform);
            plat.SetActive(false);
            platforms.Add(plat);
        });
    }

    public GameObject actualPoolPlat()
    {
        for(int i = 0; i < platforms.Count; i++)
        {
            if(!platforms[i].activeInHierarchy)
            return platforms[i];
        }

        GameObject plat = (GameObject)Instantiate(platform);
        plat.SetActive(false);
        platforms.Add(plat);
        return plat;
    }
}

