using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DeadChecker : MonoBehaviour
{
    public GameObject Carlos;
    public GameObject PlatformGenerator;

    public CinemachineVirtualCamera DeathZoom;

    private PlatformDestroyer[] platformsList;

    private EnemyController[] enemyList;

    private CoinController[] coinList;

    public Text DeathTxt;

    // Update is called once per frame
    void Update()
    {
        
        if (Input.GetKeyDown(KeyCode.R))
        {
            DeathTxt.text = "";         
            waitTime();
            GetComponent<DeadChecker>().enabled = false;
        } else
        {
            DeathTxt.text = "YOU DIED, SCORE: " + GameManager.totalScore + " PRESS 'R' TO RESTART";
            
        }
    }

    public void waitTime()
    {
        StartCoroutine("WaitXTime");
    }

    public IEnumerator WaitXTime()
    {
        GameManager.coinScore = 0;
        GameManager.enemyScore = 0;
        Carlos.SetActive(false);
        Carlos.transform.position = new Vector2(0, 3);
        yield return new WaitForSeconds(1);
        DeathZoom.Priority = 0;
        PlatformGenerator.transform.position = new Vector2(0, 0);
        destroyLastObjects();               
        Carlos.SetActive(true);
        Carlos.GetComponent<CarlosMovement>().MusicPlayer.GetComponent<AudioSource>().Play();
    }

    private void destroyLastObjects()
    {
        enemyList = FindObjectsOfType<EnemyController>();

        for (int i = 0; i < enemyList.Length; i++)
        {
            Destroy(enemyList[i].gameObject);
        }

        coinList = FindObjectsOfType<CoinController>();

        for (int i = 0; i < coinList.Length; i++)
        {
            Destroy(coinList[i].gameObject);
        }

        platformsList = FindObjectsOfType<PlatformDestroyer>();

        for (int i = 0; i < platformsList.Length; i++)
        {
            Destroy(platformsList[i].gameObject);
        }
    }
}
