using UnityEngine;

public class PlatformGenerator : MonoBehaviour
{

    public Transform generationPoint;
    private float distanceBetween;
    public static float minDistanceSpace = 1.5f;
    public static float maxDistanceSpace = 2.5f;

    private float distanceYBetween;
    public float minDistanceYSpace;
    public float maxDistanceYSpace;

    public GameObject[] platforms;

    public GameObject[] coins;

    private float[] platformWidths;
    private int randomPickerPlatfoms;

    private int randomPickerCoins;

    private float lastPosition = 8.0f;

    public GameObject EnemyPrefab;

    private bool justCheckOneTime = false;

    // Start is called before the first frame update
    void Start()
    {
        platformWidths = new float[platforms.Length];

        for (int i = 0; i < platformWidths.Length; i++)
        {
            platformWidths[i] = platforms[i].GetComponent<BoxCollider2D>().size.x;
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (transform.position.x < generationPoint.position.x)
        {
            randomPickerPlatfoms = Random.Range(0, platforms.Length);
            randomPickerCoins = Random.Range(0, coins.Length);

            distanceBetween = Random.Range(minDistanceSpace, maxDistanceSpace);
            distanceYBetween = Random.Range(minDistanceYSpace, maxDistanceYSpace);

            transform.position = new Vector2(transform.position.x + (lastPosition / 2) + (platformWidths[randomPickerPlatfoms] / 2) + distanceBetween, distanceYBetween);

            Instantiate(platforms[randomPickerPlatfoms], transform.position, transform.rotation);

            lastPosition = platformWidths[randomPickerPlatfoms];

            if (!justCheckOneTime)
            {
                int randomSpawn = Random.Range(0, 100);
                int randomPosition = Random.Range(-1, 1);

                if (randomSpawn <= 20)
                {
                    Instantiate(EnemyPrefab, new Vector2(transform.position.x + randomPosition, transform.position.y + 1), transform.rotation);
                }

                randomSpawn = Random.Range(0, 100);

                if (randomSpawn <= 50)
                {                   
                    randomPosition = (int)Random.Range(0, 2);
                    int randomPositiony = (int)Random.Range(1, 3);

                    Instantiate(coins[randomPickerCoins], new Vector2(transform.position.x + randomPosition, transform.position.y + randomPositiony), transform.rotation);
                }
            }
        }
    }
}
