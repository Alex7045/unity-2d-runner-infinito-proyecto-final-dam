using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformDestroyer : MonoBehaviour
{

    private GameObject destroy;
    // Start is called before the first frame update
    void Start()
    {
        destroy = GameObject.Find("Destroy");
    }

    // Update is called once per frame
    void Update()
    {
        try
        {
            if (transform.position.x < destroy.transform.position.x)
            {
                Destroy(gameObject);
            }
        }
        catch (Exception ignored)
        {}       
    }

}
