using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinController : MonoBehaviour
{

    public ParticleSystem coinPS;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            GameManager.coinScore += 5;
            coinPS = Instantiate(coinPS);
            coinPS.transform.position = new Vector2(transform.position.x, transform.position.y + 0.5f);
            coinPS.Play();
            Destroy(coinPS.gameObject, 1);           
            Destroy(gameObject);
        }
    }
}
