using Cinemachine;
using UnityEngine;
using UnityEngine.UI;

public class CarlosMovement : MonoBehaviour
{
    // Start is called before the first frame update�

    public float MoveSpeed = 0.0f;
    public float JumpForce = 0.0f;

    private Rigidbody2D CarlosRigidbody;

    public bool isGrounded;
    public LayerMask GroundLayer;

    private Collider2D CarlosCollider;

    private Animator CarlosAnimator;

    public float speedMultip;
    public float scoreMultip;

    private bool jumping = false;

    public Text scoreTxt;
    public Text highScoreTxt;
    public Text scoreTxtStatic;
    public Text highScoreTxtStatic;

    public Text startTxt;

    public GameObject deadChecker;

    public CinemachineVirtualCamera DeathZoom;
    public CinemachineVirtualCamera StartZoom;

    int highScore;

    public GameObject MusicPlayer;

    void Start()
    {
        Cursor.visible = false;

        Screen.lockCursor = true;

        CarlosRigidbody = GetComponent<Rigidbody2D>();

        CarlosCollider = GetComponent<Collider2D>();

        CarlosAnimator = GetComponent<Animator>();

        highScore = PlayerPrefs.GetInt("highScore", 0);
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.E))
        {
            MusicPlayer.GetComponent<AudioSource>().volume = 0.8f;
            GameManager.isStarting = false;
        }
        else
        {
            MusicPlayer.GetComponent<AudioSource>().volume = 0.2f;
            CarlosAnimator.SetFloat("Speed", 0);
            CarlosAnimator.SetBool("Grounded", true);

            scoreTxt.text = "";
            highScoreTxt.text = "";

            scoreTxtStatic.enabled = false;
            highScoreTxtStatic.enabled = false;

            startTxt.enabled = true;

            StartZoom.Priority = 30;
        }

        if (GameManager.isStarting)
        {
            return;
        }

        StartZoom.Priority = -1;

        CarlosAnimator.SetBool("Grounded", isGrounded);
        CarlosAnimator.SetFloat("Speed", CarlosRigidbody.velocity.x);

        scoreTxt.text = "" + GameManager.totalScore;
        highScoreTxt.text = "" + highScore;

        scoreTxtStatic.enabled = true;
        highScoreTxtStatic.enabled = true;

        startTxt.enabled = false;

        isGrounded = Physics2D.IsTouchingLayers(CarlosCollider, GroundLayer);

        GameManager.totalScore = (int)transform.position.x + GameManager.coinScore + GameManager.enemyScore;

        if (transform.position.x > scoreMultip)
        {
            if (MoveSpeed < 25.0f)
            {
                scoreMultip += scoreMultip * speedMultip;

                MoveSpeed *= speedMultip;
            }

            if (PlatformGenerator.minDistanceSpace < 8.0f)
            {
                PlatformGenerator.minDistanceSpace += 0.15f;
                PlatformGenerator.maxDistanceSpace += 0.15f;
            }
        }

        CarlosRigidbody.velocity = new Vector2(MoveSpeed, CarlosRigidbody.velocity.y);

        if (Input.GetKeyDown(KeyCode.Space) && isGrounded)
        {
            GetComponent<AudioSource>().Play();
            CarlosRigidbody.velocity = new Vector2(CarlosRigidbody.velocity.x, JumpForce);
            jumping = true;
        }

        if (Input.GetKeyDown(KeyCode.Space) && jumping && !isGrounded)
        {
            GetComponent<AudioSource>().Play();
            CarlosRigidbody.velocity = new Vector2(CarlosRigidbody.velocity.x, JumpForce);
            jumping = false;
        }

        if (highScore < GameManager.totalScore)
        {
            highScore = GameManager.totalScore;
            PlayerPrefs.SetInt("highScore", highScore);
            PlayerPrefs.Save();
            highScoreTxt.text = PlayerPrefs.GetInt("highScore", 0) + "";
        }

        if (transform.position.y < -1f)
        {
            DeathZoom.Priority = 30;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Fall")
        {
            dieSequence();
        }
    }

    public void dieSequence()
    {
        MusicPlayer.GetComponent<AudioSource>().volume = 0.2f;        
        DeathZoom.Priority = 30;
        scoreTxtStatic.enabled = false;
        highScoreTxtStatic.enabled = false;
        scoreTxt.text = "";
        highScoreTxt.text = "";
        GameManager.isStarting = true;
        MoveSpeed = 5;
        scoreMultip = 50;
        PlatformGenerator.minDistanceSpace = 1.5f;
        PlatformGenerator.maxDistanceSpace = 2.5f;
        GameManager.isDead = true;
        deadChecker.GetComponent<DeadChecker>().enabled = true;
        this.gameObject.SetActive(false);
    }
}
