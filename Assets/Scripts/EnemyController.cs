using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public float speed = 0.0f;
    public Transform groundChk;
    private Animator anim;
    private bool grounded;
    private bool rotated = false;
    private float ActualSpeed = 0.0f;
    private bool stop = false;

    public ParticleSystem enemyPS;

    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!stop)
        {
            grounded = Physics2D.Linecast(transform.position, groundChk.position, LayerMask.GetMask("Ground"));

            ActualSpeed = speed * Time.deltaTime;
            transform.Translate(Vector2.left * ActualSpeed);

            if (!grounded)
            {
                if (rotated)
                {
                    transform.eulerAngles = new Vector3(0, 0, 0);
                    rotated = false;
                }
                else
                {
                    transform.eulerAngles = new Vector3(0, -180, 0);
                    rotated = true;
                }
            }

        }
        else
        {
            anim.SetFloat("Speed", 0);
        }

        if (transform.position.y < -5.0f)
        {
            Destroy(gameObject);
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            GetComponent<AudioSource>().Play();
            collision.gameObject.GetComponent<CarlosMovement>().dieSequence();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            collision.GetComponent<Rigidbody2D>().velocity = new Vector2(collision.GetComponent<Rigidbody2D>().velocity.x, 5);

            enemyPS = Instantiate(enemyPS);
            enemyPS.transform.position = new Vector2(transform.position.x, transform.position.y + 0.5f);
            enemyPS.Play();
            Destroy(enemyPS.gameObject, 1);
            Destroy(gameObject);


            anim.SetBool("isDead", true);
            GetComponent<BoxCollider2D>().enabled = false;
            groundChk.gameObject.SetActive(false);
            GameManager.enemyScore += 20;
        }
    }
}